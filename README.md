# Ticketing System

> This is a basic system that should be scalable to a point that can serve as a ticketing support system for anyone that desires to use it. 

## Setup

* [Get Docker] (https://www.docker.com/get-docker)
* [Install Composer Globally] (https://getcomposer.org/doc/00-intro.md)
* [Install Node/NPM Globally] (https://nodejs.org/en/)

* Clone project
    * `git clone https://gitlab.com/quincarter/ticketingSystem.git`
    * Make a copy of the `.env.example` and name it `.env`
    * Run the following command in the project root to get started! 
        * `npm run setup:fresh`

App will be served [here] (http://localhost:10000)