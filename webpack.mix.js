 let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
        'resources/assets/js/app.js',
        'resources/assets/semantic/dist/semantic.min.js',
        'resources/assets/js/semanticActivations.js'
], 'public/js').version()
   .sass('resources/assets/sass/app.scss', 'public/css').version()
   .styles([
        'resources/assets/semantic/dist/semantic.min.css'
    ], 'public/css/all.css');

 mix.copyDirectory('resources/assets/semantic/dist/themes', 'public/css/themes');
// mix.copy('resources/assets/semantic/dist/themes', 'public/css/themes', false);
