<?php

namespace App\Http\Controllers;

use App\SupportDomain;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Settings;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    public function index($domain_id, $id)
    {
        $settingsData = DB::table('settings')
            ->where('user_id', $id)
            ->where('domain_id', $domain_id)->first();

        //if user does not have existing settings,
        // they will be set to false per the Settings Model
        if (!$settingsData) {
            $settings = new Settings();
            $settingsData = $settings->getAttributes();

            $domain = SupportDomain::findOrFail($domain_id);

            $settingsData['domain'] = $domain;
        }

        return view('support.settings', compact('settingsData'));
    }

    public function save(Request $request)
    {
        try {
            //UPDATE
            if (!Settings::where('domain_id', '=', $request->changeSetting['domain_id'])
                ->where('user_id', '=', $request->changeSetting['user_id'])
                ->update([
                    'show_devices' => $request->changeSetting['show_devices'],
                    'show_resolve_button' => $request->changeSetting['show_resolve_button'],
                    'nav_show_tickets' => $request->changeSetting['nav_show_tickets'],
                    'nav_show_register' => $request->changeSetting['nav_show_register']
                ])) {
                throw new \Exception();
            }
            return self::showSavedSettings($request);

        } catch (\Exception $e) {
            try {
                //INSERT
                $settings = new Settings();
                $settings->domain_id = $request->changeSetting['domain_id'];
                $settings->user_id = $request->changeSetting['user_id'];
                $settings->show_devices = $request->changeSetting['show_devices'];
                $settings->show_resolve_button = $request->changeSetting['show_resolve_button'];
                $settings->nav_show_tickets = $request->changeSetting['nav_show_tickets'];
                $settings->nav_show_register = $request->changeSetting['nav_show_register'];

                $settings->save();

                if (!$settings) {
                    throw new \Exception();
                } else {
                    return self::showSavedSettings($request);
                }
            } catch (\Exception $e) {
                $settings['error'] = $e;
                return $settings['error'];
            }
        }

        //return self::index($request->changeSetting['domain_id'], $request->changeSetting['user_id']);
    }

    private static function showSavedSettings(Request $request)
    {
        $userSettings = DB::table('settings')
            ->where('user_id', $request->changeSetting['user_id'])
            ->where('domain_id', $request->changeSetting['domain_id'])->first();

        return json_encode($userSettings);
    }
}
