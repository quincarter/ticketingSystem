<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TicketForm;

class TicketFormController extends Controller
{
    public function index()
    {
        $tickets = TicketForm::latest()->get();
        return view('support.ticket', compact('tickets'));
    }

    public function create(Request $request)
    {
        $input = $request->all();
        TicketForm::create($input);
    }
}
