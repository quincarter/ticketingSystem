<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $attributes = [
        'domain_id' => 0,
        'nav_show_tickets' => 0,
        'nav_show_register' => 0,
        'show_devices' => 0,
        'show_resolve_button' => 0,
    ];

    protected $fillable = [
        'domain_id',
        'user_id',
        'nav_show_tickets',
        'nav_show_register',
        'show_devices',
        'show_resolve_button',
    ];

    public function domain()
    {
        $this->belongsTo(SupportDomain::class);
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
