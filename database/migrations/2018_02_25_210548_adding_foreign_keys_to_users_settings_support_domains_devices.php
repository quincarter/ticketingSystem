<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingForeignKeysToUsersSettingsSupportDomainsDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('selected_domain_id')->references('id')->on('support_domains');
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('domain_id')->references('id')->on('support_domains');
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->foreign('device_type_id')->references('id')->on('device_types');
            $table->foreign('domain_id')->references('id')->on('support_domains');
        });

        Schema::table('device_types', function (Blueprint $table) {
            $table->foreign('domain_id')->references('id')->on('support_domains');
        });

        Schema::table('ticket_forms', function (Blueprint $table) {
            $table->foreign('domain_id')->references('id')->on('support_domains');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('device_id')->references('id')->on('devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropForeign(['role_id']);
            $table->dropForeign(['selected_domain_id']);
        });

        Schema::table('settings', function(Blueprint $table){
            $table->dropForeign(['user_id']);
            $table->dropForeign(['domain_id']);
        });

        Schema::table('devices', function(Blueprint $table){
            $table->dropForeign(['device_type_id']);
            $table->dropForeign(['domain_id']);
        });

        Schema::table('device_types', function(Blueprint $table){
            $table->dropForeign(['domain_id']);
        });

        Schema::table('ticket_forms', function(Blueprint $table){
            $table->dropForeign(['domain_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['device_id']);
        });


    }
}
