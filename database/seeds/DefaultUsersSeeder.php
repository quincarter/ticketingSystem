<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class DefaultUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = new User();
        $user_1->name = 'Domain Super Admin';
        $user_1->email = 'quin.carter@gmail.com';
        $user_1->password = Hash::make('superadmin');
        $user_1->selected_domain_id = 1;
        $user_1->role_id = 1;
        $user_1->save();
    }
}
