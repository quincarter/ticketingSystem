<?php

use Illuminate\Database\Seeder;
use App\DeviceTypes;

class DeviceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deviceTypes = new DeviceTypes();
        $deviceTypes->name = 'Computer';
        $deviceTypes->domain_id = 1;
        $deviceTypes->save();

        $deviceTypes = new DeviceTypes();
        $deviceTypes->name = 'Tablet';
        $deviceTypes->domain_id = 1;
        $deviceTypes->save();

        $deviceTypes = new DeviceTypes();
        $deviceTypes->name = 'SmartPhone';
        $deviceTypes->domain_id = 1;
        $deviceTypes->save();

        $deviceTypes = new DeviceTypes();
        $deviceTypes->name = 'Printer';
        $deviceTypes->domain_id = 1;
        $deviceTypes->save();
    }
}
