<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SupportDomainTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(DefaultUsersSeeder::class);
        //$this->call(SettingsSeeder::class);
        $this->call(DeviceTypesTableSeeder::class);
        $this->call(DevicesTableSeeder::class);
    }
}
