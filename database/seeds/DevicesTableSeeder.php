<?php

use Illuminate\Database\Seeder;
use App\Devices;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $devices = new Devices();
        $devices->name = 'Windows Laptop';
        $devices->device_type_id = 1;
        $devices->domain_id = 1;
        $devices->save();

        $devices = new Devices();
        $devices->name = 'Mac Laptop';
        $devices->device_type_id = 1;
        $devices->domain_id = 1;
        $devices->save();

        $devices = new Devices();
        $devices->name = 'iPad';
        $devices->device_type_id = 2;
        $devices->domain_id = 1;
        $devices->save();

        $devices = new Devices();
        $devices->name = 'Surface Pro 4';
        $devices->device_type_id = 2;
        $devices->domain_id = 1;
        $devices->save();

        $devices = new Devices();
        $devices->name = 'iPhone';
        $devices->device_type_id = 3;
        $devices->domain_id = 1;
        $devices->save();

        $devices = new Devices();
        $devices->name = 'Samsung Galaxy Note 4';
        $devices->device_type_id = 3;
        $devices->domain_id = 1;
        $devices->save();

        $devices = new Devices();
        $devices->name = 'Dell 5301 Laser';
        $devices->device_type_id = 4;
        $devices->domain_id = 1;
        $devices->save();
    }
}
