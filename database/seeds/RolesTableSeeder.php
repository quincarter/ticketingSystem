<?php

use Illuminate\Database\Seeder;
use App\Roles;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Roles();
        $role_admin->name = 'Super Admin';
        $role_admin->save();

        $role_user = new Roles();
        $role_user->name = 'Domain Admin';
        $role_user->save();

        $role_user = new Roles();
        $role_user->name = 'User';
        $role_user->save();
    }
}
