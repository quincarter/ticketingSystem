<?php

use Illuminate\Database\Seeder;
use App\SupportDomain;

class SupportDomainTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supportDomain = new SupportDomain();
        $supportDomain->name = 'No Support Domain Selected';
        $supportDomain->save();

        $supportDomain = new SupportDomain();
        $supportDomain->name = 'Your Company Support';
        $supportDomain->save();

        $supportDomain = new SupportDomain();
        $supportDomain->name = 'Test Company 2';
        $supportDomain->save();
    }
}
