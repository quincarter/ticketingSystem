<?php

use Illuminate\Database\Seeder;
use App\Settings;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = new Settings();
        $settings->user_id = 1;
        $settings->domain_id = 1; //0 domain is the user specific settings
        $settings->save();

        $settings = new Settings();
        $settings->user_id = 1;
        $settings->domain_id = 2;
        $settings->save();
        //the rest of the table has default values to true (1)
    }
}
