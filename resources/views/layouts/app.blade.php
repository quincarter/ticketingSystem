<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

</head>
<body>
    <div id="app">
        <div class="ui top fixed stackable green inverted menu">
            <a class="item" href="{{ url('/' . Auth::user()['selected_domain_id']) }}">
                    {{ config('app.name', 'Laravel') }}
            </a>
            @guest
                <div class="right menu">
                    <a class="item" href="{{ route('login') }}">Login</a>
                    <a class="item" href="{{ route('register') }}">Register</a>
                </div>
            @else
            <div class="right menu">
                <div class="ui item dropdown">
                    <div class="text">
                        {{ Auth::user()->name }}
                    </div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="/{{ Auth::user()['selected_domain_id'] }}/settings/{{Auth::user()->id}}">
                            <i class="cogs icon"></i> Settings
                        </a>
                        <a class="item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="sign out alternate icon"></i> Logout
                        </a>
                        <form id="logout-form" class="hidden" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <div class="divider"></div>
                        <a href="/{{Auth::user()['selected_domain_id']}}/ticketForm" class="item">
                            <i class="edit icon"></i> Create a ticket
                        </a>
                        <div class="item">
                            <span class="description">ctrl + o</span>
                            Open...
                        </div>
                        <div class="item">
                            <span class="description">ctrl + s</span>
                            Save as...
                        </div>
                        <div class="item">
                            <span class="description">ctrl + r</span>
                            Rename
                        </div>
                        <div class="item">Make a copy</div>
                        <div class="item">
                            <i class="folder icon"></i>
                            Move to folder
                        </div>
                        <div class="item">
                            <i class="trash icon"></i>
                            Move to trash
                        </div>
                        <div class="divider"></div>
                        <div class="item">Download As...</div>
                        <div class="item">
                            <i class="dropdown icon"></i>
                            Publish To Web
                            <div class="menu">
                                <div class="item">Google Docs</div>
                                <div class="item">Google Drive</div>
                                <div class="item">Dropbox</div>
                                <div class="item">Adobe Creative Cloud</div>
                                <div class="item">Private FTP</div>
                                <div class="item">Another Service...</div>
                            </div>
                        </div>
                        <div class="item">E-mail Collaborators</div>
                    </div>
                </div>
            </div>
            @endguest
        </div>
        <main id="mainContent" class="ui very padded content">
            @yield('content')
            <fab></fab>
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
