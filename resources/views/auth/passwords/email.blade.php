@extends('layouts.app')

@section('content')
    <div class="ui container">
        <div class="ui content">
            <div class="ui very padded segment">
                <div>
                    <div class="row">
                        <div class="ui header column">
                            Reset Password
                        </div>
                    </div>
                    <div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="ui form" method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                <div class="column">
                                    <input id="email" type="email"
                                           class="ui input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="column">
                                    <button type="submit" class="ui button primary">
                                        Send Password Reset Link
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
