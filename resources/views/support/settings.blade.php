@extends('layouts.app')

@section('content')
    <?php //var_dump($settingsData); ?>
    <settings-component
            :settings-data="{{ json_encode($settingsData) }}"
            :user="{{ Auth::user() }}">
    </settings-component>
@endsection