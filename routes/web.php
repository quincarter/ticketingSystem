<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => 'isLoggedIn'], function () {
    //home route
    Route::get('/{id}', 'HomeController@index')->name('home');

    //ticket routes
    Route::get('/{domain_id}/ticketForm', 'TicketFormController@index')->name('ticket');
    Route::post('/{domain_id}/ticketForm/submit', 'TicketFormController@submit')->name('submitTicket');

//settings routes
    Route::get('/{domain_id}/settings/{id}', 'SettingsController@index')->name('settings');
    Route::post('/{domain_id}/settings/{id}/save', 'SettingsController@save')->name('saveSettings');
});
